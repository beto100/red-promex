<!DOCTYPE HTML>
<head lang="es">
	<title>
		Gracias
	</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="shortcut icon" href="/favicon.ico">
	<script src="https://code.jquery.com/jquery-1.11.0.min.js"></script>
	<link rel="stylesheet" type="text/css" href="estilos.css">

	<!-- ### Google Fonts ### -->
    <link href="http://fonts.googleapis.com/css?family=Titillium+Web:400,700,600,200" rel="stylesheet" type="text/css"/>

    <!-- Analytics -->
    <?php
    	include 'template.inc';
    ?>
</head>
<body>
	<div class="mil">
		<img style="margin-top: 20px;" src="img/logo.png">
		<header style="margin-top:150px; background-image: none;">
			<h1>
				!Bien Hecho!<br> Ahora solo te queda un paso más
			</h1>
			<p style="font-size: 20px; margin-top: 30px; margin-bottom: 35px;">
				Da click a continuación para llenar un último formulario y selecciones las vacantes de tu preferencia
			</p>
			<p>
				<a id="button" style="font-size: 20px; border: 1px solid #777; padding: 10px; border-radius: 10px; display: block; width: 200px; margin: auto;" href="https://mydoforms.appspot.com/webapp?id=ag9zfm15ZG9mb3Jtcy1ocmRyGAsSC1Byb2plY3RGb3JtGICAoICW4bgIDA">
					Ir a formulario<br><span style="font-size: 14px;">Último paso</span>
				</a>
			</p>
		</header>
	</div>
	<script>
		// Using jQuery Event API v1.3
		$('#button').on('click', function() {
		  ga('send', 'event', 'External site', 'click', 'Formulario Vacante');
		});
	</script>
</body>
</html>
