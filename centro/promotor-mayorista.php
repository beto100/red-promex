<!DOCTYPE HTML>
<head lang="es">
	<title>
		Promotor Mayorista
	</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="shortcut icon" href="/favicon.ico">

    <!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
	<!-- Optional theme -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap-theme.min.css">
	
	<link rel="stylesheet" type="text/css" href="../estilos.css">
	<link rel="image_src" href="http://redpromex.com/vacantes/img/logofb.png" />
	<meta property="og:image" content="http://redpromex.com/vacantes/img/logofb.png" />
	<script>
	function validateForm()
	{
		var a=document.forms["myform"]["NAME"].value;
		var b=document.forms["myform"]["EMAIL"].value;
		var c=document.forms["myform"]["CELULAR"].value;
		var d=document.forms["myform"]["PHONE"].value;
		var e=document.forms["myform"]["AGE"].value;
		var f=document.forms["myform"]["ESCOLAR"].value;
		var g=document.forms["myform"]["MENSAJE"].value;

		if (a==null || a=="" || b==null || b=="" || c==null || c=="" || d==null || d=="" || e==null || e=="" || f==null || f=="" || g==null || g=="")
		{
			alert("Para poder contactarnos contigo, debes almenos llenar los campos obligatorios, gracias.");
			return false;
		}
	}
	</script>

	<!-- ### Google Fonts ### -->
    <link href="http://fonts.googleapis.com/css?family=Titillium+Web:400,700,600,200" rel="stylesheet" type="text/css"/>

    <!-- Analytics -->
    <?php
    	include '../template.inc';
    ?>
</head>
<body>
	<div class="container">

		<!-- HEADER -->
		<!-- ============================================================== -->
		<div class="row">
			<div class="col-sm-offset-1 col-sm-10">
				<header>
					<h1>Promotor Mayorista</h1>
					<p><img src="../img/tag.png">México</p>
					<p>23/Marzo/2015</p>
				</header>
			</div>
		</div>

		
		<div class="row">
			<div class="col-sm-12">
				<div class="border"></div>
			</div>
		</div>
		

		<!-- LEFT PART -->
		<!-- ============================================================== -->

		<div class="row">
			<div class="col-sm-12 col-md-offset-1 col-md-8 col-lg-offset-1 col-lg-8">
				<div class="left">
					<h2>
						Descripción de la empresa
					</h2>
					<p>
						Nuestro compromiso es ofrecer Servicios de Calidad en promociones punto de venta a través de personal confiable, capacitado y comprometido, de esta manera lograr los objetivos y el éxito de nuestros Clientes.
					</p>
					<p>
						Nuestra visión es ser reconocidos como una Organización sólida y líder en innovación, que se distinga por la Calidad de su Servicio a Clientes y Empleados, ofreciendo el recurso humano para promociones estratégicas en el punto de venta con el fin de lograr el éxito de nuestros clientes.
					</p>
					<p>
						Nuestra cultura de trabajo se basa en nuestros valores que llevamos a cabo cada día asegurándonos de cubrir las necesidades de todos nuestros clientes
					</p>
					<h2>
						Descripción del empleo
					</h2>
					<ul>
						<li>
							Actividades a realizar: Control de inventario, gestionar pedido con mayorista, surtir el anaquel, abordar al cliente, conocer información de la competencia (desplazamientos, exhibiciones, materiales especiales, etc.) Realizar frenteo, generar reportes de movimiento de producto, Entregar facturas, realizar cobranza.
						</li>
						<li>
							Zonas de Trabajo: MAGDALENA CONTRERAS, ECATEPEC, GAM, NAUCALPAN, ALVARO OBREGON, IZTAPALAPA, NEZAHUALCOYOTL
						</li>
						<li>
							Horario: Lunes a Sabado
						</li>
					</ul>

					<h2>
						Requisitos
					</h2>
					<ul>
						<li>
							Edad: 25 a 45 años
						</li>
						<li>
							Sexo: indistinto.
						</li>
						<li>
							Escolaridad: Bachillerato
						</li>
						<li>
							Experiencia: Mínimo de 1 año  como promotor Mayorista
						</li>
					</ul>
				</div>
			</div>

			<div class="col-sm-12 col-md-2 col-lg-2">
				<div class="right">
					<a href="https://mydoforms.appspot.com/webapp?id=ag9zfm15ZG9mb3Jtcy1ocmRyGAsSC1Byb2plY3RGb3JtGICAoJqkibkIDA">
					<div class="aplicar">
						Aplicar Aquí
					</div>
					</a>
					<div class="right2">
						<div class="subtitulo">
							<h4>
								Comparte con tus amigos este empleo
							</h4>
						</div>
						<a href="#"
							onclick="
							window.open(
							'https://www.facebook.com/sharer/sharer.php?u='+encodeURIComponent(location.href),
							'facebook-share-dialog',
							'width=626,height=436');
							return false;">
							<img src="../img/fb.png">
							</a>
						<a href="https://twitter.com/share?text=Les+comparto+esta+vacante+de+trabajo+por+Red+Promex" target="_blank"><img src="../img/tw.png"></a>
					</div>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-sm-12">
				<div class="border"></div>
			</div>
		</div>


	</div>


	<!-- Latest compiled and minified JavaScript -->
	<script src="//code.jquery.com/jquery-1.11.3.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>

</body>
</html>